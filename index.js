const playshipUtils = require('playship_utils');
const config = {
  SERVICE_NAME: 'reconcile-script',
  LOGGER: {
    LOG_LEVEL: 'debug', // project wide log level
    LOG_SRC: true,
  },
};
const helpers = playshipUtils.init(config);
const logger = helpers.getLogger();
const rds = playshipUtils.utils.rds;
const redis = helpers.getSessionRedis();
const lastNDays = 7;

rds.initConnectionPool(
    'gamezydbslave.c7q7n4ksymis.ap-south-1.rds.amazonaws.com',
    'bonusandfund',
    'gamezyroot',
    'gamezyroot',
    2,
    'bfm',
);
// rest client
const req = {logger};
helpers.getRestHelper().initInternalRestClient(req);
const intRestClient = req.internalRestClient;
const BFM_URL = 'http://internal-bfm-service-lb-prod-2006446859.ap-south-1.elb.amazonaws.com';
const PROBLEM_MATCH_ID_API = '/bfm/battle/reconcileAndSettle';
const mysqlClient = rds.getDbConnection('bfm');
const NodeSlack = require('node-slack');
const slack = new NodeSlack(
    'https://hooks.slack.com/services/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',
    {});

const refundSettled = [], refundUnsettled = [], partialRefundSettled = [],
    partialRefundUnsettled = [];

async function reconcileSettlement(battleIdList, settled, unsettled) {
  for (let battleId of battleIdList) {
    try {
      let res = await executeForMatchId(battleId);
      if (res.status === 'OK') {
        redis.srem('RECONCILE:UNSETTLED', battleId);
        settled.push(battleId);
      } else {
        unsettled.push(battleId);
      }
    } catch (e) {
      logger.error('error executing for battleId: {}', battleId, e);
    }
  }
}

const exec = async function() {
  const nonRefundMatchIds = await fetchNonRefundedMatchIds();
  let unsettledCount = await redis.sadd('RECONCILE:UNSETTLED',
      ...nonRefundMatchIds);
  logger.info(`Inserted ${unsettledCount} battleIds into session redis`);
  await reconcileSettlement(await redis.smembers('RECONCILE:UNSETTLED'),
      refundSettled, refundUnsettled);

  const parMatchIds = await fetchPartialSettlementMatchIds();
  let partialSettledCount = await redis.sadd('RECONCILE:PARTIAL_SETTLED',
      ...parMatchIds);
  logger.info(`Inserted ${partialSettledCount} battleIds into session redis`);
  await reconcileSettlement(await redis.smembers('RECONCILE:PARTIAL_SETTLED'),
      partialRefundSettled, partialRefundUnsettled);
};

async function executeForMatchId(battleId) {
  let matchId = battleId.split('_')[1];
  logger.info('executing for match', matchId);
  let res = await intRestClient.sendPostRequest(
      BFM_URL + PROBLEM_MATCH_ID_API, {matchId: matchId.toString()}, null,
      30 * 1000);
  logger.info('Result after settlement for match', matchId, 'is\n',
      JSON.stringify(res));
  return res;
}

async function fetchNonRefundedMatchIds() {
  const fetchStatement = 'SELECT distinct(battle_id) as battleId FROM '
      + 'battle_account WHERE updated_at < now() - interval 5 hour '
      + 'and updated_at >= now() - interval ? day '
      + 'and status = 1';
  let res = await mysqlClient.SELECT(fetchStatement, [lastNDays]);
  let matchIds = res.map(p => p.battleId);
  logger.info('MatchIds with non refunds are', JSON.stringify(matchIds));
  return matchIds;
}

async function fetchPartialSettlementMatchIds() {
  const fetchStatement = 'SELECT distinct(battle_id) as battleId '
      + 'FROM battle_account WHERE status IN (5, 4) and updated_at >= '
      + 'now() - interval ? day '
      + 'group by battle_id having count(1) < 2';
  let res = await mysqlClient.SELECT(fetchStatement, [lastNDays]);
  let matchIds = res.map(p => p.battleId);
  logger.info('MatchIds with partial settlement are', JSON.stringify(matchIds));
  return matchIds;
}

async function publishReport() {
  let message = `Refund settled for ${refundSettled.length}: ${refundSettled}\n`
      + `Refund pending for ${refundUnsettled.length}: ${refundUnsettled}\n`
      + `Partial refund settled for ${partialRefundSettled.length}: `
      + `${partialRefundSettled}\n`
      + `Partial refund pending for ${partialRefundUnsettled.length}: `
      + `${partialRefundUnsettled}\n`;

  slack.send({
    text: message,
    channel: 'gz-ps-application-alerts',
    username: 'Reconcile settlement Lambda',
    icon_emoji: 'male-detective',
  });
}

exports.handler = async (event) => {
  try {
    await exec();
    await publishReport();
    logger.info('Execution Done, All OK!!!!');
  } catch (e) {
    logger.error('error execution', e);
  }
};
